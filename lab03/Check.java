//Nathan Blair 2-9-18 cse2
//Check calculator that outputs cost per person
import java.util.Scanner;

public class Check {
  // main method required for every Java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );
    System.out.print("Enter the original cost of the check in the form xx.xx: "); //Print statement for user to input cost of the check
    double checkCost = myScanner.nextDouble(); //Place a scanner for user input
    System.out.print("Enter the percentage tip that you wish to pay as a whole number in the form xx: "); //Print statement for user to input cost of the check
    double tipPercent = myScanner.nextDouble(); //Place a scanner for user input
    tipPercent /= 100; //We want to convert the percentage into a decimal value
    System.out.print("Enter the number of the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt(); //Place a scanner for user input
    double totalCost; //Double for total cost
    double costPerPerson; //Double for cost per person
    int dollars, dimes, pennies;  //whole dollar amount of cost for storing digits to the right of the decimal point for the cost $ 
    totalCost = checkCost * (1 + tipPercent); //Arithmetic for total cost
    costPerPerson = totalCost / numPeople;  //get the whole amount, dropping decimal fraction
    dollars=(int)costPerPerson; //Calculation for dollars per person
    //get dimes amount, e.g., 
    // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
    //  where the % (mod) operator returns the remainder
    //  after the division:   583%100 -> 83, 27%5 -> 2 
    dimes=(int)(costPerPerson * 10) % 10; //Calculation for dimes per person
    pennies=(int)(costPerPerson * 100) % 10; //Calculation for pennies per person
    System.out.print("Each person in the group owes $ "); //Print statement 
    System.out.print(dollars); //Print statement 
    System.out.print("."); //Print statement 
    System.out.print(dimes); //Print statement 
    System.out.println(pennies); //Print statement 
    
}  //end of main method   
  	} //end of class