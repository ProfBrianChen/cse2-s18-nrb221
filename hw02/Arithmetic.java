//Nathan Blair 2-3-18 cse2
//Second homework assignment. Shopping, tax, and price practice

public class Arithmetic {
      // main method required for every Java program
     public static void main(String[] args) {

int numPants = 3;  //Number of pairs of pants
double pantsPrice = 34.98; //Cost per pair of pants\
int numShirts = 2; //Number of sweatshirts
double shirtPrice = 24.99; //Cost per shirt
int numBelts = 1; //Number of belts
double beltCost = 33.99; //cost per box of belts
double paSalesTax = 0.06; //the tax rate
       
double totalCostOfPants;   //total cost of pants
double totalCostOfShirts;   //total cost of shirts
double totalCostOfBelts;   //total cost of belts    
double totalSalesTaxPants;  //total sales tax on pants
double totalSalesTaxShirts;  //total sales tax on shirts
double totalSalesTaxBelts;  //total sales tax on belts
double totalPurchase;  //total cost of purchases
double totalSalesTax;  //total sales tax 
double totalTransaction;  //total transaction cost 

totalCostOfPants = numPants * pantsPrice; // Arithmetic for cost of pants
totalCostOfShirts = numShirts * shirtPrice; // Arithmetic for cost of shirts
totalCostOfBelts = numBelts * beltCost; // Arithmetic for cost of belts
totalSalesTaxPants = (totalCostOfPants * paSalesTax); //Arithmetic total for sales tax on pants
totalSalesTaxShirts = totalCostOfShirts * paSalesTax; //Arithmetic total for sales tax on shirts
totalSalesTaxBelts = totalCostOfBelts * paSalesTax; //Arithmetic total for sales tax on belts
totalPurchase = (totalCostOfPants + totalCostOfShirts) + totalCostOfBelts;  //Arithmetic for total purchases without taxes

//converting the sloppy values into values with two decimal places
totalSalesTaxPants = totalSalesTaxPants * 100; 
double newtotalSalesTaxPants = (int) totalSalesTaxPants;
newtotalSalesTaxPants = newtotalSalesTaxPants / 100;
       
totalSalesTaxShirts = totalSalesTaxShirts * 100; 
double newtotalSalesTaxShirts = (int) totalSalesTaxShirts;
newtotalSalesTaxShirts = newtotalSalesTaxShirts / 100;     

totalSalesTaxBelts = totalSalesTaxBelts * 100; 
double newtotalSalesTaxBelts = (int) totalSalesTaxBelts;
newtotalSalesTaxBelts = newtotalSalesTaxBelts / 100;
       
totalSalesTax = (newtotalSalesTaxPants + newtotalSalesTaxShirts) + newtotalSalesTaxBelts; //Arithmetic for total sales tax
totalTransaction = totalPurchase + totalSalesTax; //Arithmetic for total transaction

System.out.print("$ ");       
System.out.println(totalCostOfPants); //Print statemment
System.out.print("$ "); 
System.out.println(totalCostOfShirts); //Print statemment
System.out.print("$ "); 
System.out.println(totalCostOfBelts); //Print statemment
System.out.print("$ "); 
System.out.println(newtotalSalesTaxPants); //Print statemment for the new total sales tax on pants
System.out.print("$ "); 
System.out.println(newtotalSalesTaxShirts); //Print statemment for the new total sales tax on shirts
System.out.print("$ "); 
System.out.println(newtotalSalesTaxBelts); //Print statemment for the new total sales tax on belts
System.out.print("$ "); 
System.out.println(totalPurchase); //Print statemment for the total purchase
System.out.print("$ "); 
System.out.println(totalSalesTax); //Print statemment for the total sales tax
System.out.print("$ "); 
System.out.println(totalTransaction); //Print statemment for the total transaction cost
     }
}