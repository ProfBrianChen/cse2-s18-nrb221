//Nathan Blair 2-2-18 cse2
//My bicycle cyclometer measuring speed and distance. Records time elapsed and number of rotations of the front wheel. 

public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
	  int secsTrip1=480;  //time in seconds for trip one
    int secsTrip2=3220;  //time in seconds for trip two
		int countsTrip1=1561;  //number of rotations of the front wheel during trip one
		int countsTrip2=9037; //number of rotations of the front wheel during trip two
      
    double wheelDiameter=27.0,  //front wheel's diameter
  	PI=3.14159, //value of pi
  	feetPerMile=5280,  //Number of feet per mile
  	inchesPerFoot=12,   //Number of inches per foot
  	secondsPerMinute=60;  //Number of seconds per minute
	  double distanceTrip1, distanceTrip2,totalDistance;  //Total distance is distance1 + distance2
      
    System.out.println("Trip 1 took "+ 
        (secsTrip1/secondsPerMinute)+" minutes and had "+
        countsTrip1+" counts.");
	  System.out.println("Trip 2 took "+
        (secsTrip2/secondsPerMinute)+" minutes and had "+
        countsTrip2+" counts.");
        //Print out the time and number of rotations of trips 1 and 2

    distanceTrip1=countsTrip1*wheelDiameter*PI; //distance of trip one is equal to the number of rotations of the front wheel multiplied by the diameter of the wheel and pi to get the circumfrence
	  distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	  distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //distance of trip two is equal to the number of rotations of the front wheel multiplied by the diameter of the wheel and pi to get the circumfrence
	  totalDistance=distanceTrip1+distanceTrip2; //Gives distance in miles 
      
    //Print out the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles"); //Print out distance of trip 1
	  System.out.println("Trip 2 was "+distanceTrip2+" miles"); //Print out distance of trip 2
	  System.out.println("The total distance was "+totalDistance+" miles"); //Print out distance of total trip

	}  //end of main method    
} //end of class