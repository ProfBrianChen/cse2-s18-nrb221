//Nathan Blair 2-16-18 cse2
//Random card generator 1-13 represent diamonds, 14-26 clubs, followed by hearts, and then spades. The order for the generator is ace, 2, 3, ... queen, king.
import java.util.Random;

public class CardGenerator {
    // main method required for every Java program
          public static void main(String[] args) {
            Random rand = new Random(); //Random number generator
            int card = rand.nextInt(52) + 1; //Generates a integer between 1 and 52
            
            String suit = "";
            String type = "";
            int divider = 13;
            
            if (card >=1 && card <=13){
              suit = "Diamonds" ; 
            }
            else if (card >=14 && card <= 26){
              suit = "Clubs" ; 
            }              
            else if (card >=27 && card <= 39){
              suit = "Hearts";
            }           
            else if (card >=40 && card <= 52){
              suit = "Spades";
            }             
            if (card % divider == 1){
              type = "Ace";
            }
            else if (card % divider == 2){
              type = "Two";
            }
            else if (card % divider == 3){
              type = "Three";
            }
            else if (card % divider == 4){
              type = "Four";
            }
            else if (card % divider == 5){
              type = "Five";
            }
            else if (card % divider == 6){
              type = "Six";
            }
            else if (card % divider == 7){
              type = "Seven";
            }
            else if (card % divider == 8){
              type = "Eight";
            }
            else if (card % divider == 9){
              type = "Nine";
            }
            else if (card % divider == 10){
              type = "Ten";
            }
            else if (card % divider == 11){
              type = "Jack";
            }
            else if (card % divider == 12){
              type = "Queen";
            }
            else if (card % divider == 0){
              type = "King";
            }
           System.out.println("Your card is the " + type + " of " + suit);
          } //end of main method
} //end of class