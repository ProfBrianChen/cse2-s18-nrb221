import java.util.Scanner;// imports Scanner 
import java.util.Arrays;// imports Arrays
public class CSE2Linear{
    public static void main (String[]args)// Main Method
    {
        System.out.println("Pleasr enter 15 integers in ascending order: ");// prompts user to enter 15 integers
        Scanner input = new Scanner(System.in);// Scanner
        int x = 0;// counter for inputs
        int[] myArray = new int[15];// array with a length of 15
        while (x < 15) //while loop
        {
            if (input.hasNextInt())//checks input 
            {
                myArray[x] = input.nextInt();// saves as integer
                if (myArray[x] < 0 || myArray[x] > 100)// checks to see if the user input is greater than 100 or less than 0
                {
                    System.out.println("Error: not an integer");// if so then prints error and ends program
                    System.exit(0);
                }
                else// if not continues
                {
                    if (x > 0)// if the counter is greater than 0
                    {
                        if (myArray[x] < myArray[x - 1])// check to make sure the latest input is greater than the previous number
                        {
                            System.out.println("Error: Not in the correct order ");// if not then prints error and ends program
                            System.exit(0);
                        }
                    }
                x++;// increments counter
                }
            }
            else // since input isn't an int
            {
                System.out.println("Error: not an integer");// prints error message and ends program
                System.exit(0);
            }
        }
        System.out.println("Sorted: ");// prints "Sorted: "
        System.out.println(Arrays.toString(myArray));// converts array to a string statement to be printed out
        System.out.print("Enter a grade to search for: ");// tells user to enter a grade to search for
        if (input.hasNextInt())// checks to see if the user input an int
        {
            int y = input.nextInt();// if integer was input saves as int
            BinaryS(myArray, y);
            Scramble(myArray);
            System.out.println("Scrambled:");// print 
            System.out.println(Arrays.toString(myArray));// covert
            System.out.print("Enter a grade to search for: ");// prompts user to enter number 
            if (input.hasNextInt())// makes sure its an int
            {
                LinearS(myArray,y);// calls method
            }
            else
            {
                System.out.println("Error: not an integer"); //prints that there is an error
                System.exit(0);
            }
        }
        else 
        {
            System.out.println("Error: no grade found");
            System.exit(0);
        }
    }
    
    public static void LinearS(int[] myArray, int x)// LinearSearch method
    {
        for (int i = 0; i < myArray.length; i++)// loops as long as int i is less than length of array 
        {
            if (myArray[i] == x)// checks to see if the number at given position is equal to the user input
            {
                System.out.println(x + " was found in the list in " + (i + 1) + " iterations");// if so then prints where it was found
                break;
            }
            else if (myArray[i] != x && i == (myArray.length - 1))//if user input wasn't found after 15 iterations prints statement below
            {
                System.out.println(x + " was not found in " + (i + 1) + " iterations");
            }
        }
    }
    public static int[] Scramble(int[] myArray)// Scramble method
    {
        for (int i = 0; i < myArray.length; i++)// loops for the length of the array 
        {
            int x = (int)(Math.random()*myArray.length);// random number 
            int store = myArray[i];// temporarily stores number at i index in array
            while (x != i)// as long as the random number isn't equal to i switches index of i with index of random number
            {
                myArray[i] = myArray[x];
                myArray[x] = store;
                break;
            }
        }
        return myArray;
    }
    public static void BinaryS(int[] array2, int integer)
    {
        int highIndex = array2.length - 1;
        int lowIndex = 0;
        int counter = 0;
        while (lowIndex <= highIndex)
        {
            counter++;
            int midIndex = (highIndex + lowIndex)/2;
            if (integer < array2[midIndex])
            {
                highIndex = midIndex - 1;
            }
            else if (integer > array2[midIndex])
            {
                lowIndex = midIndex + 1;   
            }
            else if (integer == array2[midIndex])
            {
                System.out.println(integer + " was found in " + counter + " iterations");
                break;
            }
        }
        if (lowIndex > highIndex)
        {
            System.out.println(integer + " was not found in " + counter + " iterations");
        }
    }
}