//Program for printing a twist using loops

import java.util.Scanner;

public class TwistGenerator {
  public static void main(String[] args){
    int length = 0;
    int x = 0;
    int y = 0;
    int z = 0;
    Scanner scan = new Scanner(System.in);
    
    while (length < 1){
      System.out.print("Enter a positive integer for length " );
      if(scan.hasNextInt() == true){
        length = scan.nextInt();
      }
      else {
        String junkWord = scan.next();
      }
    }
    
    int rem = length % 3;
    
    String topLayer1 = "\\" ;
    String topLayer2 = " " ;
    String topLayer3 = "/" ;
    
    String midLayer1 = " ";
    String midLayer2 = "X";
    String midLayer3 = " ";
    
    String botLayer1 = "/";
    String botLayer2 = " ";
    String botLayer3 = "\\";
    
    String topSeg = "";
    while ( x < length){
      topSeg = topSeg.concat(topLayer1);
      x++;
      if (x == length){
        break;
      }
      topSeg = topSeg.concat(topLayer2);
      x++;
      if (x == length){
        break;
      }
      topSeg = topSeg.concat(topLayer3);
      x++;
      if (x == length){
        break;
      }
    }
    String midSeg = "";
    while ( y < length){
      midSeg = midSeg.concat(midLayer1);
      y++;
      if (y == length){
        break;
      }
      midSeg = midSeg.concat(midLayer2);
      y++;
      if (y == length){
        break;
      }
      midSeg = midSeg.concat(midLayer3);
      y++;
      if (y == length){
        break;
      }
    }
    String botSeg = "";
    while ( z < length){
      botSeg = botSeg.concat(botLayer1);
      z++;
      if (z == length){
        break;
      }
      botSeg = botSeg.concat(botLayer2);
      z++;
      if (z == length){
        break;
      }
      botSeg = botSeg.concat(botLayer3);
      z++;
      if (z == length){
        break;
      }
    }
      
    System.out.println( topSeg );
    System.out.println( midSeg );
    System.out.println( botSeg );
    
  } // end of method
} // end of class
