//This homework has the objective of giving you practice in writing methods, 
//overloading methods, and in forcing the user to enter good input. 
//Write a program that can process a string by examining all the characters, or just a specified number of characters in the string, 
//and determining if they are letters. Let the user enter a string and choose if he or she wants to examine all the characters or just a certain number.
import java.util.Scanner;

public class StringAnalysis{
  
  public static void main (String[] args){
    Scanner myScanner1 = new Scanner ( System.in); 
    System.out.print("Enter your string variable: ");
    String b = myScanner1.next(); 
    System.out.println("Enter 1 if you want to analyze the entire string and 2 if you want to enter a certain number of characters to analyze: ");
    int userDecision = myScanner1.nextInt(); 
    if (userDecision == 1){
      stringAnalysis (b);
    }
    else if (userDecision == 2 ){
      System.out.print ("Enter your string variable again: ");
      String y = myScanner1.next();
      System.out.println("Enter the number of characters you want to analyze in the string: ");
      int lengthEvaluation = myScanner1.nextInt();
      stringAnalysis (y, lengthEvaluation);
    }
  } // end of main method
  
  public static boolean stringAnalysis (String a){
    int i = 0;
    int characterCount = a.length(); 
    if (Character.isLetter(a.charAt(i))){ //checks through whole word
      if (i <= characterCount){
        i++;
        System.out.println("All characters in the string are letters.");
      }
       
    }
    else {
        System.out.println("Not all of the characters in the string are letters.");
      
  }
    return true;
} // end of string analysis method 1
  
  public static boolean stringAnalysis (String e, int lengthEvaluation){
    int j = 0;
    int characterCount1 = e.length();
    if (j >= lengthEvaluation){
      if (Character.isLetter(e.charAt(j))){ // checks up to users specified point
        j++;     
        System.out.println("All characters in selected length are letters.");
      }
    }
    return true;
  } // end of string analysis method 2
    
} // end of class

