//This homework has the objective of giving you practice in writing methods, 
//overloading methods, and in forcing the user to enter good input. 
//Write a program that can calculate the area of three different shapes; a rectangle, a triangle and a circle. 
//Let the user choose which shape they want 
//Then, have the user enter the values of the dimension(s) appropriate for each shape in the form of doubles.
import java.util.Scanner;  //import scanner

public class Area {    
    public static String checkInput (){ //method that checks the input for the correct shape
    
      Scanner scan = new Scanner(System.in);
      System.out.println("Enter: rectangle, triangle or circle");//shapes to choose from
      String shape = scan.next(); //inputed shape gets stored in String type
      boolean check = true;  //boolean used later on 
      while(check){ //will always run because boolean is initialized as true
        if (shape.equals("rectangle")){  //if statement
          break;
        }
        else if (shape.equals("triangle")){ //else if statement
          break;
        }
        else if (shape.equals("circle")){  //else if statement
          break;
        }
        else {
            System.out.println("Error when entering the shape, try again. Enter: rectangle, triangle or circle");//prompts user to input correct shape
            shape = scan.next();
        }  
       }//check shape input while loop ends
        return shape;
     }//check method that validates the chosen shape
 
  public static double rectangle (){
    Scanner scan = new Scanner(System.in);
    System.out.println("length: ");
    while(!scan.hasNextDouble()){
      System.out.println("Error, input a double");
      String junkWord = scan.nextLine();
    }
    double length = scan.nextDouble();
    
    System.out.println("width: ");
    while(!scan.hasNextDouble()){   //while loop
      System.out.println("Error, input a double");
      String junkWord = scan.nextLine();
    }
    double width = scan.nextDouble();
    
    double area = length * width;  //area formula for rectangle
    return area;
  }//rectangle method ends
  
  
   public static double triangle (){
    Scanner scan = new Scanner(System.in);
    System.out.println("length: ");
    while(!scan.hasNextDouble()){  //while loop
      System.out.println("Error, input a double");
      String junkWord = scan.nextLine();
    }
    double length = scan.nextDouble();
    
    System.out.println("base: ");
    while(!scan.hasNextDouble()){  //while loop
      System.out.println("Error, input a double");
      String junkWord = scan.nextLine();
    }
    double base = scan.nextDouble();
    
    double area = (length * base)/2;  //area formula for triangle
    return area;
  }//trinagle method ends
  
  
   public static double circle (){
    Scanner scan = new Scanner(System.in);
    System.out.println("radius: ");
    while(!scan.hasNextDouble()){
      System.out.println("Error, input a double");
      String junkWord = scan.nextLine();
    }
    double radius = scan.nextDouble();
    
    double area = Math.pow(radius,2)*3.1415;  //area formula for for circle
    return area;
  }//cirlce method ends
  
   public static void main(String [] args){
     
    String shape = checkInput() ;  //shape that will be used 
    double area = 0;
    if (shape.equals("rectangle")){  //if statement
          area = rectangle();  //assigns value stored in rectangle method to area
        }
        else if (shape.equals("triangle")){ //else if statement
          area = triangle();  //assigns value stored in triangle method to area
        }
        else if (shape.equals("circle")){  //else if statement
          area = circle();   //assigns value stored in circle method to area
        }
    System.out.println("Area of " + shape + " is: " + area );  //print statement
  }    //main method ends
  
}   //class ends

    
    
    
    