//Nathan Blair 2-13-18 cse2
//Converter for pyramid volume
import java.util.Scanner;

public class Pyramid {
    // main method required for every Java program
        public static void main(String[] args) {
        Scanner myScanner = new Scanner( System.in );
        System.out.print("Enter the square side of a pyramid(input length): "); //Print statement for user to input length
        double length = myScanner.nextDouble(); //Place a scanner for user input
        System.out.print("Enter the height of the pyramid: "); //Print statement for user to input height
        double height = myScanner.nextDouble(); //Place a scanner for user input
        
        double volume = (height * (length * length))/3; //calculation of the volume since V=(h*L*L)/3
          
        System.out.println(volume); //Print statement 
    
}  //end of main method   
  	} //end of class