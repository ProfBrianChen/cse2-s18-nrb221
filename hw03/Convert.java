//Nathan Blair 2-13-18 cse2
//Converter for hurricane precipitation
import java.util.Scanner;

public class Convert {
  // main method required for every Java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );
    System.out.print("Enter the affected area in acres: "); //Print statement for user to input area in acres
    double acres = myScanner.nextDouble(); //Place a scanner for user input
    System.out.print("Enter the average inches of rain dropped: "); //Print statement for user to input inches of rain dropped
    double inchesrain = myScanner.nextDouble(); //Place a scanner for user input
     
    double inchesPerMile = 63360; //Number of inches per mile
    double squareMilesPerAcre = 0.0015625; //NUmber of square miles per acres
    
    double land = acres * squareMilesPerAcre; //area in miles
    double rain = inchesrain/inchesPerMile; //quantity of rain in miles
    
    double squareMiles = land * rain; //square miles of rain
    
    System.out.println(squareMiles + "cubic miles"); //Print statement  
    
}  //end of main method   
  	} //end of class