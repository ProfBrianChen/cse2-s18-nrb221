//Nathan Blair 2-3-18 cse2
//First homework assignment. Welcome Screen

public class WelcomeClass {
      // main method required for every Java program
     public static void main(String[] args) {
     System.out.println("  ----------- "); //print statement
     System.out.println(" | WELCOME | "); //print statement
     System.out.println("  -----------"); //print statement
     System.out.println("  ^  ^  ^  ^  ^  ^"); //print statement
     System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\"); //print statement
     System.out.println("<-n--r--b--2--2--1->"); //print statement
     System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /"); //print statement
     System.out.println("  v  v  v  v  v  v"); //print statement
     System.out.println("  Hi my name is Nathan Blair and I like soccer,"); //print statement
     System.out.println("  skiing, video games, and all Pittsburgh sports!"); //print statement
   }
}