import java.util.Scanner;
import java.util.Random;
public class lab08 {
  public static void main(String[] args){
    Scanner scan = new Scanner(System.in);
    
    int numStudents = (int)((Math.random() * 6) + 5);
    
    String[] students;
    students = new String[numStudents];
    
    int[] midterm;
    midterm = new int[numStudents];
    
    System.out.println ("Enter " + numStudents +" student names");
    
    for(int i = 0; i < numStudents; i++){
      students[i] = scan.next(); 
    }
    for(int j = 0; j < numStudents; j++){
      int testScore = (int)((Math.random() * 100) + 1);
      midterm[j] = testScore;
      System.out.println(students[j] + ": " + midterm[j]); 
    }
    
  }
}